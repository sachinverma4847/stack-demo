# Stack Demo REST Application
This application will create a growing sized stack with default initial capacity of 10(configurable) and a load factor of 2(configurable) at the boot time. 
The stack implementation is backed by array data structure.

## To Run the application locally
This is the spring boot application. To run the app simply run main class StackDemoApplication.java

## API Details:
This application has four different API's to perform different operations on stack.  
- Push
- Pop
- Top
- Size
#### Please import the postman collection for detailed API's. Postman collection available in project resource folder at: 
`resources/templates/Stack.postman_collection.json` 

