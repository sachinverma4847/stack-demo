package com.io.imanage.controller;

import com.io.imanage.service.StackService;
import com.io.imanage.config.AppConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by Sachin Verma
 * User: s0v00eu
 * Date: 12/04/21
 */
@RestController
@RequestMapping("/stack/")
public class StackController {

    @Autowired
    private StackService stackService;

    @GetMapping("/")
    public String testApi(){
        return "Welcome!";
    }

    @GetMapping(value = "/pop", produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String,Integer> popData(){
        Map<String,Integer> response = new HashMap<>();
        response.put(AppConstant.STACK_DATA,stackService.popData());
        return response;
    }

    @PostMapping("/push/{data}")
    public void pushData(@PathVariable("data") Integer data){
        stackService.pushData(data);
    }

    @GetMapping("/size")
    public Map<String,Integer> getStackSize(){
        Map<String,Integer> response = new HashMap<>();
        response.put(AppConstant.STACK_SIZE,stackService.size());
        return response;
    }

    @GetMapping(value = "/peek", produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String,Integer> topData(){
        Map<String,Integer> response = new HashMap<>();
        response.put(AppConstant.STACK_DATA,stackService.topData());
        return response;
    }

}
