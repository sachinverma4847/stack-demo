package com.io.imanage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/*
 * Created by Sachin Verma
 * User: s0v00eu
 * Date: 12/04/21
 */

@SpringBootApplication
public class StackDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(StackDemoApplication.class, args);
	}

}
