package com.io.imanage.exception;

/*
 * Created by Sachin Verma
 * User: s0v00eu
 * Date: 13/04/21
 */
public class EmptyStackException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public EmptyStackException(String message) {
        super(message);
    }
}
