package com.io.imanage.exception;

/*
 * Created by Sachin Verma
 * User: s0v00eu
 * Date: 13/04/21
 */

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Date;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(EmptyStackException.class)
    public ResponseEntity<?> handleEmptyStackException(EmptyStackException e, WebRequest request){
        ErrorDetail errorDetail = new ErrorDetail(new Date(),e.getMessage(),((ServletWebRequest)request).getRequest().getRequestURI());
        return new ResponseEntity<>(errorDetail, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleGeneralException(Exception e, WebRequest request){
        ErrorDetail errorDetail = new ErrorDetail(new Date(),e.getMessage(),((ServletWebRequest)request).getRequest().getRequestURI());
        return new ResponseEntity<>(errorDetail, HttpStatus.NOT_FOUND);
    }
}
