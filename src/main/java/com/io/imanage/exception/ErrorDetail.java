package com.io.imanage.exception;

import java.util.Date;

/*
 * Created by Sachin Verma
 * User: s0v00eu
 * Date: 13/04/21
 */
public class ErrorDetail {

    private Date timeStamp;
    private String message;

    private String path;

    public ErrorDetail() {}

    public ErrorDetail(Date timeStamp, String message, String path) {
        this.timeStamp = timeStamp;
        this.message = message;
        this.path=path;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public String getMessage() {
        return message;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "ErrorDetail{" +
                "timeStamp=" + timeStamp +
                ", message='" + message + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
