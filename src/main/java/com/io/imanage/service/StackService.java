package com.io.imanage.service;

/*
 * Created by Sachin Verma
 * User: s0v00eu
 * Date: 12/04/21
 */

import com.io.imanage.stack.Stack;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StackService {

    @Autowired
    private Stack stack;

    public Integer popData(){
        return stack.pop();
    }

    public void pushData(Integer data){
        stack.push(data);
    }

    public Integer topData(){
        return stack.top();
    }

    public Integer size(){
        return stack.size();
    }

}
