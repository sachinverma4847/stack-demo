package com.io.imanage.config;

import com.io.imanage.stack.Stack;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/*
 * Created by Sachin Verma
 * User: s0v00eu
 * Date: 12/04/21
 */
@Configuration
public class StackConfig {

    @Value("${stack.initial.capacity}")
    private Integer initialCapacity;

    @Value("${stack.load.factor}")
    private Float loadFactor;

    @Bean
    public Stack getStack(){
        if (initialCapacity == null || initialCapacity < 1)
            initialCapacity=AppConstant.DEFAULT_INITIAL_CAPACITY;
        if (loadFactor == null || loadFactor < 1)
            loadFactor=AppConstant.DEFAULT_LOAD_FACTOR;
        return new Stack(initialCapacity,loadFactor);
    }
}


