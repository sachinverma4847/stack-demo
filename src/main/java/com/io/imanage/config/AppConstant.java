package com.io.imanage.config;

/*
 * Created by Sachin Verma
 * User: s0v00eu
 * Date: 14/04/21
 */
public class AppConstant {
    public static final String STACK_SIZE = "size";
    public static final String STACK_DATA = "data";
    public static final Integer DEFAULT_INITIAL_CAPACITY= 2;
    public static final Float DEFAULT_LOAD_FACTOR = 10f;
}
