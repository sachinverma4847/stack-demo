package com.io.imanage.stack;

/*
 * Created by Sachin Verma
 * User: s0v00eu
 * Date: 12/04/21
 */

import com.io.imanage.exception.EmptyStackException;
import static com.io.imanage.config.AppConstant.*;

public class Stack {

    private Integer[] data;

    private Integer top;

    private int capacity;

    private int size;

    private float loadFactor;

    public Stack(){
        top = -1;
        capacity= DEFAULT_INITIAL_CAPACITY;
        data = new Integer[capacity];
        loadFactor= DEFAULT_LOAD_FACTOR;
    }

    public Stack(int initialCapacity, float loadFactor){
        top = -1;
        capacity=initialCapacity;
        data = new Integer[capacity];
        this.loadFactor = loadFactor;
    }

    public Integer pop(){
        if (isEmpty()) throw new EmptyStackException("Stack is empty!");
        size--;
        return data[top--];
    }
    public void push(Integer value){
        if (top >= capacity-1)
            expand();
        data[++top] = value;
        size++;
    }
    public boolean isEmpty(){
        return top<0;
    }

    public Integer top(){
        if (this.isEmpty()) throw new EmptyStackException("Stack is empty!");
        return data[top];
    }

    public int size(){
        return size;
    }

    private void expand(){
        Integer[] temp = data;
        capacity = (int) (capacity*loadFactor);
        data = new Integer[capacity];
        for (int i=0;i<temp.length;i++)
            data[i] = temp[i];
    }
}
