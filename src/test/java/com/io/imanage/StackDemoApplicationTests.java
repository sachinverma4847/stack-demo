package com.io.imanage;

import com.io.imanage.exception.EmptyStackException;
import com.io.imanage.service.StackService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

@SpringBootTest
class StackDemoApplicationTests {

	@Autowired
	private StackService stackService;

	@Test
	void verifyStackOperations() {
		Assert.isTrue(stackService.size()==0,"Initial Stack size must be 0");
		boolean flag = false;
		try {
			stackService.popData();
		} catch (EmptyStackException e) {
			flag=true;
		}
		Assert.isTrue(flag,"Pop operation on Empty stack must throw an exception");

		stackService.pushData(10);
		Assert.isTrue(stackService.size()==1,"Stack size must be 1");
		Assert.isTrue(stackService.topData()==10,"Stack top must be 10");

		stackService.pushData(20);
		Assert.isTrue(stackService.size()==2,"Stack size must be 2");
		Assert.isTrue(stackService.topData()==20,"Stack top must be 20");

		stackService.pushData(30);
		Assert.isTrue(stackService.size()==3,"Stack size must be 3");
		Assert.isTrue(stackService.topData()==30,"Stack top must be 30");

		Assert.isTrue(stackService.popData()==30,"Stack pop must be 30");
		Assert.isTrue(stackService.size()==2,"Stack size must be 2");
		Assert.isTrue(stackService.topData()==20,"Stack top must be 20");

		Assert.isTrue(stackService.popData()==20,"Stack pop must be 20");
		Assert.isTrue(stackService.size()==1,"Stack size must be 1");
		Assert.isTrue(stackService.topData()==10,"Stack top must be 10");

		Assert.isTrue(stackService.popData()==10,"Stack pop must be 10");
		Assert.isTrue(stackService.size()==0,"Stack size must be 0");

	}

}
